<?php
class EventsController extends AppController {
    function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('index', 'view');
        
        if ( $this->Auth->user('id') ) {
            $this->Auth->allow('attending');
        }
    }

	function index() {
        $this->paginate['order'] = 'Event.created DESC, Event.event_date DESC';
        $events = $this->paginate();
        if ( $this->Auth->user('id') ) {
            foreach ($events as $key => $event) {
                $events[$key]['Event']['attending'] = false;
                foreach ($event['EventParticipant'] as $participant) {
                    if ( $participant['user_id'] == $this->Auth->user('id') ) {
                        $events[$key]['Event']['attending'] = true;
                        $events[$key]['Event']['registration_no'] = $participant['registration_no'];
                        break;
                    }
                }
            }
        }
		$this->set('events', $events);
	}

	function view($id = null) {
        $event = $this->Event->read(null, $id);
        if ( $this->Auth->user('id') ) {
            $event['Event']['attending'] = false;
            foreach ($event['EventParticipant'] as $participant) {
                if ( $participant['user_id'] == $this->Auth->user('id') ) {
                    $event['Event']['attending'] = true;
                    $event['Event']['registration_no'] = $participant['registration_no'];
                    break;
                }
            }
        }
		if (empty($event)) {
			$this->Session->setFlash(__('Invalid event', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('event', $event);
	}
    
    function dashboard($id = null) {
        $this->Event->Behaviors->attach('Containable');
        $event = $this->Event->find('first', array(
            'conditions' => array(
                'Event.id' => $id
            ),
            'contain' => array(
                'EventParticipant' => array(
                    'User'
                )
            )
        ));
		if (empty($event)) {
			$this->Session->setFlash(__('Invalid event', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('event', $event);
    }

	function add() {
		if (!empty($this->data)) {
			$this->Event->create();
			if ($this->Event->save($this->data)) {
				$this->Session->setFlash(__('Event berhasil disimpan', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Event tidak berhasil disimpan. Coba lagi!', true));
			}
		}
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid event', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Event->save($this->data)) {
				$this->Session->setFlash(__('Event berhasil disimpan', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Event tidak berhasil disimpan. Coba lagi!', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Event->read(null, $id);
		}
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid event', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Event->delete($id)) {
			$this->Session->setFlash(__('Event berhasil dihapus', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Event tidak berhasil dihapus', true));
		$this->redirect(array('action' => 'index'));
	}
    
    function attending($id = null) {
        if (!$id) {
			$this->Session->setFlash(__('Invalid event', true));
			$this->redirect(array('action'=>'index'));
		}
        
        // cek jika peserta ini sudah terdaftar atau belum
        $registered = $this->Event->EventParticipant->find('count', array(
            'conditions' => array(
                'event_id' => $id,
                'user_id' => $this->Auth->user('id')
            ), 'recursive' => -1
        ));
        $url = '/events/view/' . $id;
        if ( $registered ) {
            $this->Session->setFlash('Anda sudah terdaftar di event ini');
            $this->redirect( $url );
        }
        
        $this->Event->EventParticipant->create();
        $data = array(
            'event_id' => $id,
            'user_id' => $this->Auth->user('id')
        );
        if ($this->Event->EventParticipant->save($data)) {
			$this->Session->setFlash(__('Anda terdaftar datang di event ini', true));
		} else {
            $this->Session->setFlash(__('Anda tidak berhasil terdaftar datang di event ini', true));
        }
		$this->redirect( $url );
    }
}
?>
