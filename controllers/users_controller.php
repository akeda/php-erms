<?php
class UsersController extends AppController {
    function beforeFilter() {
        $this->Auth->authenticate = ClassRegistry::init('User');
        
        $this->Auth->allow('register', 'login');
        if ( $this->Auth->user('type') == 'member' ) {
            $this->Auth->deny('index', 'add', 'delete', 'edit');
            $this->Auth->allow('profile', 'view', 'logout');
        }
        
        $this->Auth->fields = array(
            'username' => 'email',
            'password' => 'password'
        );
        parent::beforeFilter();
    }
    
	function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('user', $this->User->read(null, $id));
	}
    
	function add() {
		if (!empty($this->data)) {
            if ( !empty($this->data['User']['password_confirm']) ) {
                $this->data['User']['password_confirm'] = $this->Auth->password($this->data['User']['password_confirm']);
                $this->User->create();
                if ($this->User->save($this->data)) {
                    $this->Session->setFlash(__('User berhasil disimpan', true));
                    $this->redirect(array('action' => 'index'));
                } else {
                    $this->Session->setFlash(__('User tidak berhasil disimpan. Coba lagi!', true));
                }
            } else {
                $this->User->validationErrors['password_confirm'] = 'Wajib diisi';
                unset($this->data['User']['password']);
                unset($this->data['User']['password_confirm']);
                $this->Session->setFlash(__('Tidak behasil mendaftar. Coba lagi!', true));
            }
		}
	}
    
    function register() {
        if ( $this->Auth->user('id') ) {
            $this->redirect(array('action' => 'profile'));
        }
        
		if (!empty($this->data)) {
            if ( !empty($this->data['User']['password_confirm']) ) {
                $this->data['User']['password_confirm'] = $this->Auth->password($this->data['User']['password_confirm']);
                $this->User->create();
                if ($this->User->save($this->data)) {
                    $this->Auth->login($this->data);
                    $this->Session->setFlash(__('Behasil terdaftar', true));
                    $url = $this->Cookie->read('Previous.url');
                    if ( substr($url, 0, 12)  == '/users/login' ) {
                        $url = '/users/profile';
                    }
                    $this->redirect($url);
                } else {
                    unset($this->data['User']['password']);
                    unset($this->data['User']['password_confirm']);
                    $this->Session->setFlash(__('Tidak behasil mendaftar. Coba lagi!', true));
                }
            } else {
                $this->User->validationErrors['password_confirm'] = 'Wajib diisi';
                unset($this->data['User']['password']);
                unset($this->data['User']['password_confirm']);
                $this->Session->setFlash(__('Tidak behasil mendaftar. Coba lagi!', true));
            }
		}
	}
    
    function login() {
        if ( $this->Auth->user('id') ) {
            $url = $this->Cookie->read('Previous.url');
            if ( substr($url, 0, 12)  == '/users/login' ) {
                $url = '/users/profile';
            }
            $this->redirect($url);
        }
    }
    
    function logout() {
        $this->Cookie->delete('Previous');
        $this->Cookie->delete('Last');
        $this->redirect($this->Auth->logout());
    }

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('User berhasil disimpan.', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('User tidak berhasil disimpan. Coba lagi!', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
	}
    
    function profile() {
        $id = $this->Auth->user('id');
        if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('controller' => 'events', 'action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->User->save($this->data)) {
				$this->Session->setFlash(__('Profil berhasil diupdate.', true));
			} else {
				$this->Session->setFlash(__('Profil tidak berhasil diupdate. Coba lagi!', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->User->read(null, $id);
		}
    }

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid user', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->User->delete($id)) {
			$this->Session->setFlash(__('User berhasil dihapus', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('User tidak berhasil dihapus', true));
		$this->redirect(array('action' => 'index'));
	}
}
?>
