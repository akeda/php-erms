<?php
class User extends AppModel {
	var $name = 'User';
	var $displayField = 'name';
	var $validate = array(
		'type' => array(
			'inlist' => array(
				'rule' => array('inlist', array('member', 'admin')),
				'message' => 'Pilih member atau admin',
				'allowEmpty' => true,
				'required' => false,
                'on' => 'create'
			),
		),
        'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Wajib diisi',
				'allowEmpty' => false,
				'required' => true
			),
		),
        'password' => array(
            'notEmpty' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'Wajib diisi'
            ),
            'confirm' => array(
                'rule' => 'vPassword',
                'message' => 'Password tidak sama'
            )
        ),
		'gender' => array(
			'inlist' => array(
				'rule' => array('inlist', array('M', 'F')),
				'message' => 'Pilih Laki-laki atau perempuan',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'pob' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Wajib diisi',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'dob' => array(
			'date' => array(
				'rule' => array('date'),
				'message' => 'Tanggal tidak valid',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'address' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Wajib diisi',
				'allowEmpty' => false,
				'required' => true
			),
			'minlength' => array(
				'rule' => array('minlength', 10),
				'message' => 'Minimal 10 karakter',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'phone' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'No. telp tidak valid',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				'message' => 'Email tidak valid',
				'allowEmpty' => false,
				'required' => true
			),
		),
	);
    
	var $hasMany = array(
		'EventParticipant' => array(
			'className' => 'EventParticipant',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

    function vPassword($field) {
        if ( isset($this->data[$this->alias]['password_confirm']) && ($this->data[$this->alias]['password_confirm'] != $field['password']) ) {
            $this->validationErrors['password_confirm'] = 'Password tidak sama';
            return false;
        }
        
        return true;
    }
}
?>
