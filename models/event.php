<?php
class Event extends AppModel {
	var $name = 'Event';
	var $displayField = 'name';
	var $validate = array(
		'code' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Wajib diisi',
				'required' => true
			),
			'maxlength' => array(
				'rule' => array('maxlength', 3),
				'message' => 'Maksimal 3 karakter',
				'allowEmpty' => false,
				'required' => false
			),
			'minlength' => array(
				'rule' => array('minlength', 3),
				'message' => 'Minimal 3 karakter',
				'allowEmpty' => false,
				'required' => false
			),
		),
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Wajib diisi',
				'allowEmpty' => false,
				'required' => true
			),
			'maxlength' => array(
				'rule' => array('maxlength', 100),
				'message' => 'Maksimal 100 karakter',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'description' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Wajib diisi',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'event_date' => array(
			'date' => array(
				'rule' => array('date'),
				'message' => 'Tanggal harus valid',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'quota' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Harus numerik',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'start_date' => array(
			'date' => array(
				'rule' => array('date'),
				'message' => 'Tanggal harus valid',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'end_date' => array(
			'date' => array(
				'rule' => array('date'),
				'message' => 'Tanggal harus valid',
				'allowEmpty' => false,
				'required' => true
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $hasMany = array(
		'EventParticipant' => array(
			'className' => 'EventParticipant',
			'foreignKey' => 'event_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
?>
