<?php
class EventParticipant extends AppModel {
	var $name = 'EventParticipant';
	//The Associations below have been created with all possible keys, those that are not needed can be removed

	var $belongsTo = array(
		'Event' => array(
			'className' => 'Event',
			'foreignKey' => 'event_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
    
    function beforeSave($created) {
        if ($created) {
            $event = $this->Event->find('first', array(
                'conditions' => array(
                    'id' => $this->data['EventParticipant']['event_id']
                ),
                'recursive' => -1, 'fields' => array('code', 'quota')
            ));
            $event_code = $event['Event']['code'];
            $quota = $event['Event']['quota'];
            
            $counter = $this->Event->find('count', array(
                'conditions' => array(
                    'id' => $this->data['EventParticipant']['event_id']
                ),
                'recursive' => -1
            ));
            
            $this->data['EventParticipant']['registration_no'] = $event_code . date('His') . str_pad($counter, strlen($quota), '0', STR_PAD_LEFT);
        }
        
        return true;
    }
}
?>
