<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php __('ERMS:'); ?>
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->css('reset');
		echo $this->Html->css('text');
		echo $this->Html->css('960');
		echo $this->Html->css('style');
		echo $this->Javascript->link('jquery');

		echo $scripts_for_layout;
	?>
</head>
<body>
<div class="top_head">
    <div class="container_12">
        <div class="grid_12 login">
            <div class="grid_9 alpha">
                <?php if ($showLoginOnTop):?>
                <?php echo $this->Form->create('User', array('controller' => 'users', 'action' => 'login'));?>
                    <?php echo $this->Form->input('email', array('div' => false, 'label' => 'Email &nbsp; '));?>
                    &nbsp;
                    <?php echo $this->Form->input('password', array('div' => false, 'label' => 'Password &nbsp; '));?>
                <?php echo $this->Form->end(array('label' => 'Login', 'div' => false));?>
                <?php endif;?>
            </div>
            <div class="grid_3 omega">
                <?php if ($showRegisterOnTop):?>
                <span>
                    <strong>Belum punya account?
                    <?php echo $this->Html->link('Register', array('controller' => 'users', 'action' => 'register'));?>
                    </strong>
                </span>
                <?php endif;?>
            </div>
            <div class="clear"></div>
            
            <? if ($showProfile):?>
                <strong><?php echo 'Hi, ' . $profile['User']['name'] . '. ';?></strong>
                <?php echo $this->Html->link('Profile', array('controller' => 'users', 'action' => 'profile'));?> |
                <?php
                    if ($isAdmin):
                        echo $this->Html->link('Daftar anggota', array('controller' => 'users', 'action' => 'index')). ' | ';
                        echo $this->Html->link('Buat event', array('controller' => 'events', 'action' => 'add')). ' | ';
                    endif;
                ?>
                <?php echo $this->Html->link('Logout', array('controller' => 'users', 'action' => 'logout'));?>
            <?php endif;?>
        </div>
        <div class="clear"></div>
    </div>
</div>

<div class="container_12">
    <h1><?php echo $this->Html->link('Event and Registration Management System', '/');?></h1>
</div>

<div class="container_12">
    <?php echo $this->Session->flash();?>
    <?php echo $this->Session->flash('auth');?>
</div>
<div class="container_12 events">
    <?php echo $content_for_layout;?>
</div>
<div id="footer">
    <p>Copyright &copy; 2010 ERMS (Event and Registration Management System)</p>
    <p>MIT Licensed</p>
</div>
<?php echo $this->element('sql_dump'); ?>
</body>
</html>
