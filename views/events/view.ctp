<div class="grid_12">
    <div class="grid_3 alpha event_bar">
        <div class="box_bar">
            <div class="event_date">
                <p><?php echo $time->format('d M Y', $event['Event']['event_date']); ?></p>
            </div>
            <div class="event_registration">
                <span class="label">Pendaftaran</span>
                <span class="val"><?php echo $time->format('d M Y', $event['Event']['start_date']);?> &mdash; <?php echo $time->format('d M Y', $event['Event']['end_date']);?></span>
                <span class="register">
                    <?php if ($time->wasYesterday($event['Event']['end_date'])):?>
                        Sudah tutup
                    <?php else:?>
                        <?php if ( isset($event['Event']['attending']) && $event['Event']['attending'] ):?>
                            <?php echo 'Anda sudah terdaftar dengan no registrasi : <strong>' . $event['Event']['registration_no'] . '</strong>';?>
                        <?php else:?>
                            <?php echo $this->Html->link('Daftar', array('controller' => 'events', 'action' => 'attending', $event['Event']['id']));?>
                        <?php endif;?>
                    <?php endif;?>
                </span>
            </div>
        </div>
    </div>
    <div class="grid_9 omega event_info">
        <div class="box_main">
            <h2><?php echo $event['Event']['name'];?></h2>
            <p class="info">
            <?php echo $event['Event']['description']; ?>
            </p>
            <p>
                <?php
                    if ( !empty($event['Event']['image_filename']) ):
                        echo $this->Html->img($event['Event']['image_filename'], array(
                            'alt' => $event['Event']['name']
                        ));
                    endif;
                ?>
            </p>
            <p class="meta">
                <span class="label">Quota</span>
                <span class="val"><?php echo $event['Event']['quota']; ?></span> &mdash;
                <span class="val">
                <?php
                    $participants = count($event['EventParticipant']);
                    if ( $participants > 0 ):
                        echo $participants . ' orang mendaftar di event ini';
                    else:
                        echo 'Belum ada yang mendaftar';
                    endif;
                ?>
                </span> &mdash;
                <span class="label">Biaya</span>
                <span class="val">
                <?php
                    if ( $event['Event']['payment'] > 0 ) {
                        echo 'Rp ' . number_format($event['Event']['payment'], 2, '.', ',');
                    } else {
                        echo 'Gratis';
                    }
                ?>
                </span>
            </p>
            <?php if ($isAdmin):?>
            <p class="manage_event">
                <span class=""><?php echo $this->Html->link('Edit', array('controller' => 'events', 'action' => 'edit', $event['Event']['id']));?></span> |
                <span class=""><?php echo $this->Html->link('Statistik', array('controller' => 'events', 'action' => 'dashboard', $event['Event']['id']));?></span>
            </p>
            <?php endif;?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>
