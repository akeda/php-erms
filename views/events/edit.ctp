<div class="grid_12 form">
<?php echo $this->Form->create('Event');?>
	<fieldset>
 		<legend>Edit event</legend>
	<?php
		echo $this->Form->input('code', array('label' => 'Kode event'));
		echo $this->Form->input('name', array('label' => 'Nama event'));
		echo $this->Form->input('description', array('label' => 'Deskripsi'));
		echo $this->Form->input('image_filename');
		echo $this->Form->input('event_date', array(
            'label' => 'Tanggal event',
            'minYear' => date('Y'), 'maxYear' => (date('Y')+5),
            'dateFormat' => 'DMY'
        ));
		echo $this->Form->input('quota');
		echo $this->Form->input('start_date', array(
            'label' => 'Pembukaan pendaftaran',
            'minYear' => date('Y'), 'maxYear' => (date('Y')+5),
            'dateFormat' => 'DMY'
        ));
		echo $this->Form->input('end_date', array(
            'label' => 'Penutupan pendaftaran',
            'minYear' => date('Y'), 'maxYear' => (date('Y')+5),
            'dateFormat' => 'DMY'
        ));
		echo $this->Form->input('payment', array(
            'label' => 'Biaya pembayaran<br />Kosongkan jika gratis'
        ));
        echo '';
	?>
	</fieldset>
<?php echo $this->Form->end(__('Update', true));?>
</div>
<div class="clear"></div>

<div class="grid_12 actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $this->Form->value('Event.id')), null, sprintf(__('Are you sure you want to delete # %s?', true), $this->Form->value('Event.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Events', true), array('action' => 'index'));?></li>
	</ul>
</div>
<div class="clear"></div>
