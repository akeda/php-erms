<div class="grid_12 form">
<?php echo $this->Form->create('Event');?>
    <div class="grid_3 alpha">
        <div class="box_bar">
            <div class="event_registration">
                <h3>Tanggal event dan pendaftaran</h3>
                <?php
                    echo $this->Form->input('event_date', array(
                        'label' => 'Tanggal event',
                        'minYear' => date('Y'), 'maxYear' => (date('Y')+5),
                        'dateFormat' => 'DMY'
                    ));
                    echo $this->Form->input('start_date', array(
                        'label' => 'Pembukaan pendaftaran',
                        'minYear' => date('Y'), 'maxYear' => (date('Y')+5),
                        'dateFormat' => 'DMY'
                    ));                    
                    echo $this->Form->input('end_date', array(
                        'label' => 'Penutupan pendaftaran',
                        'minYear' => date('Y'), 'maxYear' => (date('Y')+5),
                        'dateFormat' => 'DMY'
                    ));
                ?>
            </div>
            <div class="event_registration">
                <h3>Quota dan biaya</h3>
                <?php
                    echo $this->Form->input('quota', array(
                        'class' => 'short'
                    ));
                    echo $this->Form->input('payment', array(
                        'label' => 'Biaya pembayaran',
                        'class' => 'medium'
                    ));
                ?>
            </div>
        </div>
    </div>
    <div class="grid_9 omega">
        <div class="box_main">
            <fieldset>
                <legend>Event baru</legend>
            <?php
                echo $this->Form->input('name', array(
                    'label' => 'Nama event', 'class' => 'long'
                ));
                echo $this->Form->input('code', array(
                    'label' => 'Kode event', 'class' => 'short'
                ));
                echo $this->Form->input('description', array('label' => 'Deskripsi'));
                echo $this->Form->input('image_filename');
                echo $this->Form->submit('Simpan');
                
            ?>
            </fieldset>
        </div>
    </div>
    <div class="clear"></div>
<?php echo $this->Form->end();?>
</div>
<div class="clear"></div>

<div class="grid_12 actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Events', true), array('action' => 'index'));?></li>
	</ul>
</div>
<div class="clear"></div>
