<div class="grid_12">
    <div class="grid_3 alpha event_bar">
        <div class="box_bar">
            <div class="event_date">
                <p><?php echo $time->format('d M Y', $event['Event']['event_date']); ?></p>
            </div>
            <div class="event_registration">
                <span class="label">Pendaftaran</span>
                <span class="val"><?php echo $time->format('d M Y', $event['Event']['start_date']);?> &mdash; <?php echo $time->format('d M Y', $event['Event']['end_date']);?></span>
            </div>
        </div>
    </div>
    <div class="grid_9 omega event_info">
        <div class="box_main">
            <h2><?php echo $event['Event']['name'];?></h2>
            <p class="info">
            <?php echo $event['Event']['description']; ?>
            </p>
            <p>
                <?php
                    if ( !empty($event['Event']['image_filename']) ):
                        echo $this->Html->img($event['Event']['image_filename'], array(
                            'alt' => $event['Event']['name']
                        ));
                    endif;
                ?>
            </p>
            <p class="meta">
                <span class="label">Quota</span>
                <span class="val"><?php echo $event['Event']['quota']; ?></span> &mdash;
                <span class="label">Biaya</span>
                <span class="val">
                <?php
                    if ( $event['Event']['payment'] > 0 ) {
                        echo 'Rp ' . number_format($event['Event']['payment'], 2, '.', ',');
                    } else {
                        echo 'Gratis';
                    }
                ?>
                </span>
            </p>
            <?php if ($isAdmin):?>
            <p class="manage_event">
                <span class=""><?php echo $this->Html->link('Edit', array('controller' => 'events', 'action' => 'edit', $event['Event']['id']));?></span>
            </p>
            <?php endif;?>
        </div>
    </div>
    <div class="clear"></div>
</div>
<div class="clear"></div>

<div class="grid_12">
    <div class="box_full">
        <h2 class="participant_info">Ada <strong><?php echo count($event['EventParticipant']);?></strong> anggota menyatakan hadir.</h2>
        <table>
        <thead>
            <th>No</th>
            <th>Nama</th>
            <th>No Registrasi</th>
            <th>Email</th>
            <th>No Telp</th>
            <th>Reputasi</th>
            <th>Kehadiran</th>
        </thead>
        <tbody>
        <?php $no = 1;?>
        <?php foreach ($event['EventParticipant'] as $participant):?>
            <tr>
                <td><?php echo $no++;?></td>
                <td><?php echo $this->Html->link($participant['User']['name'], array('controller' => 'users', 'action' => 'edit', $participant['User']['id']));?></td>
                <td><?php echo $participant['registration_no'];?></td>
                <td><?php echo $participant['User']['email'];?></td>
                <td><?php echo $participant['User']['phone'];?></td>
                <td><?php echo '';?></td>
                <td><?php echo '';?></td>
            </tr>
        <?php endforeach;?>
        </tbody>
        </table>
    </div>
</div>
<div class="clear"></div>
