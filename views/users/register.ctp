<div class="grid_12 users form">
<?php echo $this->Form->create('User');?>
	<fieldset>
 		<legend>Registrasi</legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('password');
		echo $this->Form->input('password_confirm', array(
            'label' => 'Konfirmasi password', 'required' => true, 'type' => 'password',
            'div' => array(
                'class' => 'input password required'
            )
        ));
        echo $this->Html->div('input required');
        echo $this->Html->tag('label', 'Jenis kelamin');
		echo $this->Form->radio('gender', array(
                'M' => 'Laki-laki',
                'F' => 'Perempuan'
        ), array('legend' => false));
        echo '</div>';
        
		echo $this->Form->input('pob', array('label' => 'Tempat lahir'));
		echo $this->Form->input('dob', array('label' => 'Tgl lahir',
            'minYear' => 1950, 'maxYear' => (date('Y')-10),
            'dateFormat' => 'DMY'
        ));
		echo $this->Form->input('address', array('label' => 'Alamat'));
		echo $this->Form->input('phone', array('label' => 'No. telp'));
		echo $this->Form->input('email');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Register', true));?>
</div>
<div class="clear"></div>
