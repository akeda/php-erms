<div class="grid_12 users form">
<?php echo $this->Form->create('User');?>
	<fieldset>
 		<legend>Profile</legend>
	<?php
		echo $this->Form->input('name');
		echo $this->Form->radio('gender', array(
                'M' => 'Laki-laki',
                'F' => 'Perempuan'
        ), array('legend' => 'Jenis kelamin'));
		echo $this->Form->input('pob', array('label' => 'Tempat lahir'));
		echo $this->Form->input('dob', array('label' => 'Tgl lahir',
            'minYear' => 1950, 'maxYear' => (date('Y')-10),
            'dateFormat' => 'DMY'
        ));
		echo $this->Form->input('address', array('label' => 'Alamat'));
		echo $this->Form->input('phone', array('label' => 'No. telp'));
		echo $this->Form->input('email');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Update', true));?>
</div>
<div class="clear"></div>
