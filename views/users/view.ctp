<div class="grid_12 users view">
<h2><?php  __('User');?></h2>
	<dl><?php $i = 0; $class = ' class="altrow"';?>
		<dt<?php if ($i % 2 == 0) echo $class;?>>Nama</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $user['User']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>>Jenis kelamin</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php
                if ($user['User']['gender'] == 'M' ):
                    echo 'Laki-laki';
                else:
                    echo 'Perempuan';
                endif;
            ?>
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class;?>>Waktu daftar</dt>
		<dd<?php if ($i++ % 2 == 0) echo $class;?>>
			<?php echo $time->format('d/m/Y H:i:s', $user['User']['created']);?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="clear"></div>
