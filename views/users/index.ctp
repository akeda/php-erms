<div class="grid_12 users index">
	<h2><?php __('Users');?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th>No</th>
			<th><?php echo $this->Paginator->sort('name');?></th>
			<th><?php echo $this->Paginator->sort('Jenis Kelamin', 'gender');?></th>
			<th>TTL</th>
			<th>Kontak</th>
			<th><?php echo $this->Paginator->sort('Waktu register', 'created');?></th>
			<th class="actions"><?php __('Actions');?></th>
	</tr>
	<?php
	$i = 0;
	foreach ($users as $user):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $i; ?>&nbsp;</td>
		<td>
            <?php
                echo $user['User']['name'] . ' (';
                if ( $user['User']['type'] == 'admin' ):
                    echo '<strong>Administrator</strong>';
                else:
                    echo 'Member';
                endif;
                echo ')';
            ?>
        </td>
		<td>
        <?php
            if ( $user['User']['gender'] == 'M' ):
                echo 'Laki-laki';
            else:
                echo 'Perempuan';
            endif;
        ?>
        </td>
		<td><?php echo $user['User']['pob'] . ', ' . $time->format('d/m/Y', $user['User']['dob']); ?>&nbsp;</td>
		<td>
            <?php echo $user['User']['address']; ?><br />
            <?php echo $user['User']['phone']; ?><br />
            <?php echo $user['User']['email']; ?>
        </td>
        
		<td><?php echo $time->format('d M, Y H:i', $user['User']['created']);?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View', true), array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Edit', true), array('action' => 'edit', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Delete', true), array('action' => 'delete', $user['User']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $user['User']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page %page% of %pages%, showing %current% records out of %count% total, starting on record %start%, ending on %end%', true)
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('<< ' . __('previous', true), array(), null, array('class'=>'disabled'));?>
	 | 	<?php echo $this->Paginator->numbers();?>
 |
		<?php echo $this->Paginator->next(__('next', true) . ' >>', array(), null, array('class' => 'disabled'));?>
	</div>
</div>
<div class="clear"></div>

<div class="grid_12 actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New User', true), array('action' => 'add')); ?></li>
	</ul>
</div>
<div class="clear"></div>
